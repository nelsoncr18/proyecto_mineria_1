USE ProyectoSA

SELECT * FROM ProyectoSA.dbo.DIRECCIONES

SELECT * FROM ProyectoSA.dbo.DEPARTAMENTOS
SELECT * FROM ProyectoSA.dbo.EMPLEADOS
SELECT * FROM ProyectoSA.dbo.CLIENTES
SELECT * FROM ProyectoSA.dbo.PRODUCTOS
SELECT * FROM ProyectoSA.dbo.VENTAS
SELECT * FROM ProyectoDW.dbo.DIM_CLIENTE
SELECT * FROM ProyectoDW.dbo.DIM_DIRECCION
SELECT * FROM ProyectoDW.dbo.DIM_SUCURSAL
SELECT * FROM ProyectoDW.dbo.DIM_EMPLEADO
SELECT * FROM ProyectoDW.dbo.DIM_DEPARTAMENTO
SELECT * FROM ProyectoDW.dbo.DIM_PRODUCTO


-- CARGA DE DATOS DW
select 
c.codigo cedula,
concat(c.nombre, ' ', c.apellidos) nombre_completo,
c.telefono telefono,
case when c.sexo='F' then 'MUJER'
 when c.sexo='M' then 'HOMBRE'
 end sexo,
d.nombre direccion,
c.edad edad
from ProyectoSA.dbo.CLIENTES c
inner join ProyectoSA.dbo.DIRECCIONES d
on c.DIRECCION= d.codigo


-- DW DIRECCION
select 
codigo,
nombre descripcion

from ProyectoSA.dbo.DIRECCIONES


-- DW EMPLEADOS
select 
e.cedula identificacion,
concat(e.nombre, ' ', e.apellidos) nombre_completo,
e.telefono, 
e.salario,
e.puesto,
e.edad,
d.nombre departamento
from ProyectoSA.dbo.EMPLEADOS e
inner join ProyectoSA.dbo.DEPARTAMENTOS d
on e.DEPARTAMENTO= d.codigo



--DW PRODUCTOS
select 
p.codigo,
p.nombre descripcion,
p.precio,
p.utilidad,
p.proveedor
from ProyectoSA.dbo.PRODUCTOS p
inner join ProyectoSA.dbo.PROVEEDORES pv
on p.proveedor= pv.codigo;




-- DW DEPARTAMENTOS
SELECT * FROM ProyectoSA.dbo.DEPARTAMENTOS
SELECT * FROM ProyectoDW.dbo.DIM_DEPARTAMENTO

select 
codigo,
nombre descripcion
from ProyectoSA.dbo.DEPARTAMENTOS 

--DW SUCURSAL
SELECT * FROM ProyectoSA.dbo.SUCURSALES
SELECT * FROM ProyectoDW.dbo.DIM_SUCURSAL

select 
codigo,
descripcion nombre
from ProyectoSA.dbo.SUCURSALES 


---------------proveedor---------------
SELECT * FROM ProyectoSA.dbo.PROVEEDORES
SELECT * FROM ProyectoDW.dbo.DIM_PROVEEDOR

select 
codigo,
nombre

from ProyectoSA.dbo.PROVEEDORES


---------------TIEMPO---------------
SELECT * FROM ProyectoDW.dbo.DIM_TIEMPO

begin
declare @fecha_inicio date
declare @fecha_fin date
declare @mes int
declare @nmes nvarchar(20)
declare @annio int

begin 
select @fecha_inicio = min(fecha), 
@fecha_fin = max(fecha) from ProyectoSA.dbo.VENTAS

while @fecha_inicio <= @fecha_fin
begin 
select @mes = DATEPART(MONTH, @fecha_inicio),
@nmes = DATENAME(MONTH, DATEADD(MONTH, MONTH(@fecha_inicio) - 1,@fecha_inicio)),
@annio = DATEPART(YEAR, @fecha_inicio)

insert into ProyectoDW.dbo.DIM_TIEMPO (fecha, mes, N_MES, annio)
values  (@fecha_inicio,@mes,@nmes,@annio)
set @fecha_inicio = DATEADD(DAY,1,@fecha_inicio);
end
end
end


---TABLA DE HECHOS VENTAS--------
SELECT * FROM ProyectoSA.dbo.CLIENTES
SELECT * FROM ProyectoDW.dbo.H_VENTAS

insert into ProyectoDW.dbo.H_VENTAS(
id_cliente,
--id_sucursal,
id_producto,
id_tiempo,
--id_empleado,
--id_proveedor,
--id_direccion,
id_departamento,
cantidad,
monto_venta
)

select
c.id_cliente,
--s.id_sucursal,
p.id_producto,
t.id_tiempo,
--id_empleado,
--id_proveedor,
--id_direccion,
d.id_departamento,
count(1)cantidad,
sum(monto) monto_venta

from ProyectoSA.dbo.VENTAS v
inner join ProyectoDW.dbo.DIM_CLIENTE c
on v.cedula = c.CEDULA
--inner join ProyectoDW.dbo.DIM_SUCURSAL s
--on v.SUCURSAL = s.CODIGO
left join ProyectoDW.dbo.DIM_PRODUCTO p
on v.PRODUCTO = p.CODIGO
inner join ProyectoDW.dbo.DIM_TIEMPO t
on cast(v.FECHA as date) = cast(t.FECHA as date)
left join ProyectoDW.dbo.DIM_DEPARTAMENTO d
on v.DEPARTAMENTO = d.codigo
group by
c.id_cliente,
--s.id_sucursal,
p.id_producto,
t.id_tiempo,
--id_empleado,
--id_proveedor,
--id_direccion,
d.id_departamento


TRUNCATE TABLE EMPLEADOS

TRUNCATE TABLE CLIENTES

TRUNCATE TABLE ProyectoDW.dbo.DIM_DEPARTAMENTO

TRUNCATE TABLE PRODUCTOS

TRUNCATE TABLE PROVEEDORES

TRUNCATE TABLE SUCURSALES

TRUNCATE TABLE VENTAS