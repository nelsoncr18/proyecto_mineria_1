create or replace procedure genera_datos(cantidad in number) is
    
  vcodigo ADMIN.CLIENTES.codigo%type;
  vnombre ADMIN.CLIENTES.nombre%type;
  vapellidos ADMIN.CLIENTES.apellidos%type;
  vtelefono ADMIN.CLIENTES.telefono%type;
  vdireccion ADMIN.CLIENTES.direccion%type;
  vedad ADMIN.CLIENTES.edad%type;

begin

  for datos in 1 .. cantidad loop

    select codigo
      into vcodigo
      from (select codigo from ADMIN.CLIENTES order by dbms_random.value)
     where rownum = 1;

    select nombre
      into vnombre
      from (select nombre from ADMIN.CLIENTES order by dbms_random.value)
     where rownum = 1; 

    select apellidos
      into vapellidos
      from (select apellidos from ADMIN.CLIENTES order by dbms_random.value)
     where rownum = 1;

    select direccion
      into vdireccion
      from (select direccion from ADMIN.CLIENTES order by dbms_random.value)
     where rownum = 1;

    select (100 + abs(mod(dbms_random.random, 100)))
      into vedad
      from dual;

    select (10000000 + abs(mod(dbms_random.random, 10000000)))
      into vtelefono
      from dual;

    insert into CLIENTES
      (codigo, nombre,apellidos, telefono,direccion,edad)
    values
      (vcodigo, vnombre,vapellidos,vtelefono,vdireccion,vedad);

    end loop;
end;